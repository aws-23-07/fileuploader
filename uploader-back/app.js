const express = require('express');
const multer = require('multer');
const app = express();
const port = 3000;
const cors =require('cors');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads/');
  },
  filename: (req, file, cb) => {
    const fileName = `${Date.now()}-${file.originalname}`;
    cb(null, fileName);
  },
});

const upload = multer({ storage });

app.use(express.static('public'));

app.use(cors());

app.post('/upload', upload.single('file'), (req, res) => {
  res.json({ file: req.file });
});

app.get('/files', (req, res) => {
  const uploadDirectory = 'uploads/';
  fs.readdir(uploadDirectory, (err, files) => {
    if (err) {
      console.error(err);
      res.status(500).send('Error reading the upload directory.');
    } else {
      res.json({ files });
    }
  });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});