import React, { useState } from'react';

const FileUploader = () => {
  const [file, setFile] = useState(null);
  const [uploadedFiles, setUploadedFiles] = useState([]);

  const handleFileChange = (event) => {
    setFile(event.target.files[0]);
  };

  const handleUpload = () => {
    const formData = new FormData();
    formData.append('file', file);

    fetch('http://localhost:3000/upload', {
      method: 'POST',
      body: formData,
    })
     .then((response) => response.json())
     .then((data) => {
        setUploadedFiles((prevFiles) => [...prevFiles, data.file]);
      })
     .catch((error) => console.error(error));
  };

  return (
    <div>
      <h1>File Uploader</h1>
      <input type="file" onChange={handleFileChange} />
      <button onClick={handleUpload}>Upload File</button>
      <ul>
        {uploadedFiles.map((file, index) => (
          <li key={index}>{file.originalname}</li>
        ))}
      </ul>
    </div>
  );
};

export default FileUploader;