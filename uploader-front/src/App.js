import React from'react';
import FileUploader from './FileUploader';

function App() {
  return (
    <div>
      <FileUploader />
    </div>
  );
}

export default App;